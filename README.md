# Connect Four Template

A start of a project implementing a variant of the game "Connect Four".

## Mission

1. Extend the existing code such that the GreedyPlayer works.
2. Implement a strong Opponent using MinMax/Negamax with fixed depth (like 10).
3. Implement a perfect Player and optimize it for speed.

## Resources

- https://de.wikipedia.org/wiki/Minimax-Algorithmus#Implementierung
- https://de.wikipedia.org/wiki/Alpha-Beta-Suche#Implementierung
- https://en.wikipedia.org/wiki/Zobrist_hashing

## Selbstevaluation
Ich habe nun einige Erfahrung mit Java und bin auch ein wenig vertraut
mit der Entwicklung von Spielen. Ich verstehe die Konzepte 
des Zustandsmanagements und der 
Benutzerinteraktion, die für ein Spiel wie Vier Gewinnt erforderlich sind. 
Ich bin auch in der Lage, sauberen (aktuell alles in einer Klasse, 
wenn es ein richtig produktives Projekt wäre MUSS man es logisch aufteilen in eigene
Klassen) und effizienten Code zu schreiben, 
der die Regeln des Spiels implementiert und die Benutzererfahrung 
verbessert.

Anfangs war das Thema minMaxAlgo recht verwirrend. Jedoch konnte
ich es im Verlauf dieses Projektes immer mehr nachvollziehen. Leider konnte ich
alpha Beta zeitlich nicht fertig implementieren.

Im Allgemein ist das Endergebnis gut, wir haben einen simplen
isWinning, eine evaluate Methode und den MinMaxAlgo. Dies sollte
für den Anfang reichen um ein spannendes Game zu gestalten.

Selbsteinschätzung Note: 4.75
