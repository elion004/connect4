package ch.bbw.m411.connect4;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

/**
 * Plays a game of Connect Four on a 4x7 board (a variation of the original 6x7 board).
 * The pieces fall straight down, occupying the lowest available space within the column.
 */
public class Connect4ArenaMain {

    static final int WIDTH = 7;

    static final int HEIGHT = 4;

    static final int NOMOVE = -1;

    public static int bestMove;

    public static int initialFreeFields;

    public static int maxDepth = 4;

    //Alpha
    static int MAX = 1000;

    //Beta
    static int MIN = -1000;

    public static void main(String[] args) {
        new Connect4ArenaMain().play(new HumanPlayer(), new SmartPlayer());
    }

    static String toDebugString(Stone[] board) {
        var sb = new StringBuilder();
        for (int r = 0; r < HEIGHT; r++) {
            for (int c = 0; c < WIDTH; c++) {
                var value = board[r * WIDTH + c];
                sb.append(value == null ? "." : (value == Stone.RED ? "X" : "O"));
            }
            sb.append("-");
        }
        return sb.toString();
    }

    Connect4Player play(Connect4Player red, Connect4Player blue) {
        if (red == blue) {
            throw new IllegalStateException("must be different players (simply create two instances)");
        }
        var board = new Stone[WIDTH * HEIGHT];
        red.initialize(Arrays.copyOf(board, board.length), Stone.RED);
        blue.initialize(Arrays.copyOf(board, board.length), Stone.BLUE);
        var lastMove = NOMOVE;
        var currentPlayer = red;
        for (int round = 0; round < board.length; round++) {
            var currentColor = currentPlayer == red ? Stone.RED : Stone.BLUE;
            System.out.println(HumanPlayer.toPrettyString(board) + currentColor + " to play next...");
            lastMove = currentPlayer.play(lastMove);
            if (lastMove < 0 || lastMove >= WIDTH * HEIGHT ||
                    board[lastMove] != null && (lastMove < WIDTH || board[lastMove - WIDTH] != null)) {
                throw new IllegalStateException("cannot play to position " + lastMove + " @ " + toDebugString(board));
            }
            board[lastMove] = currentColor;
            if (isWinning(board, currentColor)) {
                System.out.println(
                        HumanPlayer.toPrettyString(board) + "...and the winner is: " + currentColor + " @ " + toDebugString(board));
                return currentPlayer;
            }
            currentPlayer = currentPlayer == red ? blue : red;
        }
        System.out.println(HumanPlayer.toPrettyString(board) + "...it's a DRAW @ " + toDebugString(board));
        return null; // null implies a draw
    }

    public static boolean isWinning(Stone[] board, Stone forColor) {

        //vertical check
        for (int i = 0; i <= 6; i++) {
            if (board[i] == forColor && board[i + 7] == forColor && board[i + (7 * 2)] == forColor && board[i + (7 * 3)] == forColor) {
                return true;
            }
        }

        //horizontal check
        for (int i = 0; i < 28; i += 7) {
            for (int x = 0; x < 4; x++) {
                if (board[x + i] == forColor && board[(x + 1) + i] == forColor && board[(x + 2) + i] == forColor && board[(x + 3) + i] == forColor) {
                    return true;
                }
            }
        }

        //diagonal-right check
        for (int i = 0; i < 4; i++) {
            if (board[i] == forColor && board[i + 8] == forColor && board[i + (8 * 2)] == forColor && board[i + (8 * 3)] == forColor) {
                return true;
            }
        }

        //diagonal-left check
        for (int i = 6; i > 2; i--) {
            if (board[i] == forColor && board[i + 6] == forColor && board[i + (6 * 2)] == forColor && board[i + (6 * 3)] == forColor) {
                return true;
            }
        }

        return false;
    }

    public enum Stone {
        RED, BLUE;

        public Stone opponent() {
            return this == RED ? BLUE : RED;
        }
    }

    public interface Connect4Player {

        /**
         * Called before the game starts and guaranteed to only be called once per livetime of the player.
         *
         * @param board       the starting board, usually an empty board.
         * @param colorToPlay the color of this player
         */
        void initialize(Stone[] board, Stone colorToPlay);

        /**
         * Perform a next move, will only be called if the Game is not over yet.
         * Each player has to keep an internal state of the 4x7 board, wher the 0-index is on the bottom row.
         * The index-layout looks as:
         * <pre>
         * 30 31 32 33 34 35 36
         * 14 15 16 17 18 19 29
         *  7  8  9 10 11 12 13
         *  0  1  2  3  4  5  6
         * </pre>
         *
         * @param opponendPlayed the last index where the opponent played to (in range 0 - width*height exclusive)
         *                       or -1 if this is the first move.
         * @return an index to play to (in range 0 - width*height exclusive)
         */
        int play(int opponendPlayed);
    }

    /**
     * An abstract helper class to keep track of a board (and whatever we or the opponent played).
     */
    public abstract static class DefaultPlayer implements Connect4Player {

        Stone[] board;

        Stone myColor;

        @Override
        public void initialize(Stone[] board, Stone colorToPlay) {
            this.board = board;
            myColor = colorToPlay;
        }

        @Override
        public int play(int opponendPlayed) {
            if (opponendPlayed != NOMOVE) {
                board[opponendPlayed] = myColor.opponent();
            }
            var playTo = play();
            board[playTo] = myColor;
            return playTo;
        }

        /**
         * Givent the current {@link #board}, find a suitable position-index to play to.
         *
         * @return the position to play to as defined by {@link Connect4Player#play(int)}.
         */
        abstract int play();

    }

    public static class HumanPlayer extends DefaultPlayer {

        static String toPrettyString(Stone[] board) {
            var sb = new StringBuilder();
            for (int r = HEIGHT - 1; r >= 0; r--) {
                for (int c = 0; c < WIDTH; c++) {
                    var index = r * WIDTH + c;
                    if (board[index] == null) {
                        if (index < WIDTH || board[index - WIDTH] != null) {
                            sb.append("\033[37m" + index + "\033[0m ");
                            if (index < 10) {
                                sb.append(" ");
                            }
                        } else {
                            sb.append("\033[37m.\033[0m  ");
                        }
                    } else if (board[index] == Stone.RED) {
                        sb.append("\033[1;31mX\033[0m  ");
                    } else {
                        sb.append("\033[1;34mO\033[0m  ");
                    }
                }
                sb.append("\n");
            }
            return sb.toString();
        }

        @Override
        int play() {
            System.out.println("where to to put the next " + myColor + "?");
            var scanner = new Scanner(System.in, StandardCharsets.UTF_8);
            return Integer.parseInt(scanner.nextLine());
        }

    }

    public static class GreedyPlayer extends DefaultPlayer {

        @Override
        int play() {
            for (int c = 0; c < WIDTH; c++) {
                for (int r = 0; r < HEIGHT; r++) {
                    var index = r * WIDTH + c;
                    if (board[index] == null) {
                        return index;
                    }
                }
            }
            throw new IllegalStateException("cannot play at all");
        }
    }

    public static class SmartPlayer extends DefaultPlayer {

        @Override
        int play() {
            bestMove = -1;
            initialFreeFields = (int) Arrays.stream(board)
                    .filter(Objects::isNull)
                    .count();
            minmax(myColor, maxDepth, initialFreeFields, board, MIN, MAX);
            return bestMove;
        }
    }

    public static long minmax(Stone currentPosition, int depth, int freeFields, Stone[] board,
                              int alpha, int beta) {
        //check winner
        if (isWinning(board, currentPosition.opponent())) {
            return -1000;
        }

        //No playable fields anymore
        if (freeFields == 0) {
            return 0;
        }

        //check risk
        if (depth == 0) {
            var myCounter = evaluate(board, currentPosition);
            var opponentCounter = evaluate(board, currentPosition.opponent());

            return myCounter - opponentCounter;
        }

        //Set default bad value
        long bestValue = -100000;

        //Get all playable moves
        var playableMoves = getPlayableMovesBoard(board);

        for (var playableMove : playableMoves) {
            board[playableMove] = currentPosition; // play a stone
            var currentValue = -minmax(currentPosition.opponent(), depth - 1, freeFields - 1, board, alpha, beta);
            board[playableMove] = null; // revert the last move
            if (depth == maxDepth) {
                System.out.println(currentValue + " pos:" + playableMove);
            }
            if (currentValue > bestValue) {
                bestValue = currentValue;
                if (depth == maxDepth) {
                    bestMove = playableMove;
                }
            }
            //Tried to implement alpha Beta, didn't work
            //Start AlphaBeta------------------------------------

            //var newBestMove = Math.max(bestMove, currentValue);
            //alpha = (int) Math.max(alpha, newBestMove);

            //if (beta <= alpha) {
            //    System.out.println("Beta: " + beta + "alpha: " + alpha + "pos: " + playableMove);
            //    bestMove = playableMove;
            //}

            //END AlphaBeta----------------------------------------
        }

        return bestValue;
    }

    public static int evaluate(Stone[] board, Stone myColor) {
        var counter = 0;
        var counterValue = 0;

        for (var stone : board) {
            counter++;
            if (stone == myColor) {

                //check low risk
                if (counter == 0 || counter == 7 || counter == 14 || counter == 21 ||
                        counter == 6 || counter == 13 || counter == 20 || counter == 27) {
                    counterValue += 3;
                }

                //check mid risk
                if (counter == 1 || counter == 8 || counter == 15 || counter == 22 ||
                        counter == 5 || counter == 12 || counter == 19 || counter == 26) {
                    counterValue += 4;
                }

                //check mid risk
                if (counter == 2 || counter == 9 || counter == 16 || counter == 23 ||
                        counter == 4 || counter == 11 || counter == 18 || counter == 25) {
                    counterValue += 5;
                }

                //check no chance risk
                if (counter == 3 || counter == 10 || counter == 17 || counter == 24) {
                    counterValue += 7;
                }
            }
        }

        return counterValue;
    }

    public static int[] getPlayableMovesBoard(Stone[] board) {
        var possibleMoves = new int[7];
        var nextFreeIndex = 0;

        for (int i = 0; i < board.length; i++) {
            if (nextFreeIndex > 6) {
                continue;
            }
            if (i <= 6) {
                if (board[i] == null) {
                    possibleMoves[nextFreeIndex] = i;
                    nextFreeIndex++;
                }
            }

            if (i > 6) {
                //is playable position?
                if (board[i] == null) {
                    //Is underneath something?
                    if (board[i - 7] != null) {
                        possibleMoves[nextFreeIndex] = i;
                        nextFreeIndex++;
                    }
                }
            }
        }

        return possibleMoves;
    }

}
